# Xvest

Source code of the BEP20 token XVEST  
Based on OpenZeppelin V4.0 contracts --> https://github.com/OpenZeppelin/openzeppelin-contracts  

Name : XVEST  
Symbol : XVEST  
Total Supply : 10.000.000.000  
Decimals : 16  
Contract address : 0x7305509e301d001696043c6bea3c0be16d12c706  

Not Mintable  
Not Pausable  
No Snapshots  
Burnable  
Ownable  

HOW TO USE IT ?  
Install [Trust Wallet](https://play.google.com/store/apps/details?id=com.wallet.crypto.trustapp&hl=fr&gl=US) on your smartphone  
Create your wallet  
Click at the top right to add a new token  
Enter this contract address : 0x7305509e301d001696043c6bea3c0be16d12c706  

