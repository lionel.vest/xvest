// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "contracts/access/Ownable.sol";

contract XVEST is ERC20Burnable, Ownable  {
    constructor() ERC20("XVEST", "XVEST") {
        _mint(msg.sender, 10000000000 * (10**uint256(decimals())));
    }
}
